package com.gitlab.drosseau.testgattclient

import android.bluetooth.BluetoothGattService
import android.content.*
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.IBinder
import android.view.*
import android.widget.ArrayAdapter
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_services_list.*
import kotlinx.android.synthetic.main.simple_uuid_item.view.*

class ServicesListActivity : AppCompatActivity() {

    private var mService: BLEService? = null
    private val mSConn = SConn()
    private var mConnected = false
    private lateinit var mDeviceAddress: String
    private lateinit var mReceiver: BroadcastReceiver
    private lateinit var mServicesAdapter: ServicesAdapter

    companion object {
        const val EXTRA_DEVICE_ADDRESS = "EXTRA_DEVICE_ADDRESS"

        private const val RESOURCE_ID = R.layout.simple_uuid_item
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_services_list)
        if (!intent.hasExtra(EXTRA_DEVICE_ADDRESS)) {
            logE("Need $EXTRA_DEVICE_ADDRESS extra when creating activity")
            finish()
            return
        }
        mDeviceAddress = intent.getStringExtra(EXTRA_DEVICE_ADDRESS)

        deviceAddress.text = mDeviceAddress
        connectedValue.setText(R.string.false_text)
        mServicesAdapter = ServicesAdapter(this)
        servicesList.adapter = mServicesAdapter
        servicesList.setOnItemClickListener { _, _, serviceIdx, _ ->
            val service = mServicesAdapter.getItem(serviceIdx)
            val intent = Intent(this, ServiceActivity::class.java)
            intent.putExtra(ServiceActivity.EXTRA_SERVICE, service)
            intent.putExtra(ServiceActivity.EXTRA_DEVICE_ADDRESS, mDeviceAddress)
            startActivity(intent)
        }
        mReceiver = InnerReceiver()

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.services_list, menu)
        menu?.let {
            if (mConnected) {
                it.findItem(R.id.menuConnect).setVisible(false)
                it.findItem(R.id.menuDisconnect).setVisible(true)
            } else {
                it.findItem(R.id.menuConnect).setVisible(true)
                it.findItem(R.id.menuDisconnect).setVisible(false)
            }
        }
        return true
    }

    private fun disconnect() {
        logD("Disconnecting")
        mConnected = false
        mServicesAdapter.clear()
        mServicesAdapter.notifyDataSetChanged()
        val intent = BLEService.makeBroadcastIntent(this, BLEService.ACTION_DISCONNECT_DEVICE)
        sendBroadcast(intent)
    }

    private fun connect() {
        logD("Connecting to $mDeviceAddress")
        mConnected = true
        val intent = BLEService.makeBroadcastIntent(this, BLEService.ACTION_CONNECT_DEVICE)
        intent.putExtra(BLEService.EXTRA_DEVICE_ADDRESS, mDeviceAddress)
        sendBroadcast(intent)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        item?.let {
            when(it.itemId) {
                R.id.menuDisconnect -> {
                    disconnect()
                }
                R.id.menuConnect -> {
                    connect()
                }
            }
            invalidateOptionsMenu()
        }
        return true
    }

    override fun onStart() {
        super.onStart()
        val intent = Intent(this, BLEService::class.java)
        bindService(intent, mSConn, 0)
    }

    override fun onStop() {
        super.onStop()
        unbindService(mSConn)
    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(mReceiver)
    }

    override fun onResume() {
        super.onResume()
        mService?.let {
            if (!it.isConnected()) {
                mServicesAdapter.clear()
                mServicesAdapter.notifyDataSetChanged()
                updateConnected(false)
            }
        }
        val filter = IntentFilter()
        filter.addAction(BLEService.ACTION_GATT_CONNECTED)
        filter.addAction(BLEService.ACTION_GATT_DISCONNECTED)
        filter.addAction(BLEService.ACTION_GATT_SERVICES_DISCOVERED)
        registerReceiver(mReceiver, filter, CLIENT_PERMISSION, null)
    }

    private inner class SConn : ServiceConnection {
        override fun onServiceConnected(cmp: ComponentName?, binder: IBinder?) {
            binder!!.let {
                mService = (binder as BLEService.LocalBinder).getService()
            }
        }

        override fun onServiceDisconnected(cmp: ComponentName?) {
            mService = null
        }
    }

    private fun setServices(services: List<BluetoothGattService>) {
        mServicesAdapter.clear()
        mServicesAdapter.addAll(services)
        mServicesAdapter.notifyDataSetChanged()
        for (s in services) {
            logD("Service: ${s.uuid}")
            for (c in s.characteristics) {
                logD("\tCharacteristic: ${c.uuid} Props: ${Integer.toBinaryString(c.properties)} Permissions: ${Integer.toBinaryString(c.permissions)} Service: ${c.service}")
                for (d in c.descriptors) {
                    logD("\t\tDescriptor: ${d.uuid} Permissions: ${Integer.toBinaryString(d.permissions)} Service: ${d.characteristic.service} Characteristic: ${d.characteristic}")
                }
            }
        }
    }

    private fun updateConnected(con: Boolean) {
        mConnected = con
        if (con) {
            connectedValue.text = getString(R.string.true_text)
        } else {
            connectedValue.text = getString(R.string.false_text)
        }
        invalidateOptionsMenu()
    }

    private inner class InnerReceiver : BroadcastReceiver() {
        override fun onReceive(ctx: Context?, intent: Intent?) {
            intent?.let {
               when (it.action) {
                   BLEService.ACTION_GATT_CONNECTED -> {
                       updateConnected(true)
                   }
                   BLEService.ACTION_GATT_DISCONNECTED -> {
                       mServicesAdapter.clear()
                       mServicesAdapter.notifyDataSetChanged()
                       updateConnected(false)
                   }
                   BLEService.ACTION_GATT_SERVICES_DISCOVERED -> {
                       logD("Services discovered")
                       mService?.let { bleService ->
                           val services = bleService.getServices()
                           for (s in services) {
                               logI("Discovered service ${s.uuid}")
                           }
                               setServices(services)
                           }
                       }
                   else -> {
                       logD("Unhandled action ${it.action}")
                   }
               }
            }
        }
    }

    private inner class ServicesAdapter(ctx: Context?) : ArrayAdapter<BluetoothGattService>(ctx, RESOURCE_ID) {
        private val mInflater = LayoutInflater.from(this@ServicesListActivity)
        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val service = getItem(position)
            val view: View
            val vh = if (convertView == null) {
                view = mInflater.inflate(RESOURCE_ID, parent, false)
                val vh = ViewHolder(view.uuid)
                view.tag = vh
                vh
            } else {
                view = convertView
                val vh = convertView.tag as ViewHolder
                vh
            }
            vh.serviceUuid.text = service.uuid.toString()
            return view
        }
    }
    private data class ViewHolder(val serviceUuid: TextView)
}

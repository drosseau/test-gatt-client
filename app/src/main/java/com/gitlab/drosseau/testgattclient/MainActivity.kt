package com.gitlab.drosseau.testgattclient

import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.*
import android.widget.ArrayAdapter
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.device_item.view.*

class MainActivity : AppCompatActivity() {

    private var mScanning = false
    private lateinit var mDevicesAdapter: DevicesAdapter

    companion object {
        private const val RESOURCE_ID = R.layout.device_item
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mDevicesAdapter = DevicesAdapter(this)
        devicesList.adapter = mDevicesAdapter
        devicesList.setOnItemClickListener { _, _, idx, _ ->
            stopScanning()
            val device = mDevicesAdapter.getItem(idx).device
            val intent = Intent(this, ServicesListActivity::class.java)
            intent.putExtra(ServicesListActivity.EXTRA_DEVICE_ADDRESS, device.address)
            startActivity(intent)
        }
        val intent = Intent(this, BLEService::class.java)
        startService(intent)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        menu?.let {
            if (!mScanning) {
                it.findItem(R.id.menuStop).setVisible(false)
                it.findItem(R.id.menuScan).setVisible(true)
            } else {
                it.findItem(R.id.menuStop).setVisible(true)
                it.findItem(R.id.menuScan).setVisible(false)
            }
        }
        return true
    }

    private fun stopScanning() {
        if (mScanning) {
            logD("Sending scan end intent")
            mScanning = false
            val intent = BLEService.makeBroadcastIntent(this, BLEService.ACTION_END_SCANNING)
            sendBroadcast(intent)
            invalidateOptionsMenu()
        }
    }

    private fun startScanning() {
        if (!mScanning) {
            logD("Sending scan begin intent")
            mScanning = true
            mDevicesAdapter.clear()
            val intent = BLEService.makeBroadcastIntent(this, BLEService.ACTION_BEGIN_SCANNING)
            sendBroadcast(intent)
            invalidateOptionsMenu()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        item?.let {
            when(it.itemId) {
                R.id.menuStop -> {
                    stopScanning()
                }
                R.id.menuScan -> {
                    startScanning()
                }
                else -> {
                    return super.onOptionsItemSelected(item)
                }
            }
        }
        return true
    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(mReceiver)
    }

    override fun onResume() {
        super.onResume()

        val filter = IntentFilter()
        filter.addAction(BLEService.ACTION_DEVICE_DISCOVERED)
        registerReceiver(mReceiver, filter, CLIENT_PERMISSION, null)
    }

    private inner class DevicesAdapter(mContext: Context?) : ArrayAdapter<WrappedDevice>(mContext, RESOURCE_ID) {
        private val mInflater = LayoutInflater.from(this@MainActivity)
        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val wrapped = getItem(position)
            val view: View
            val vh = if (convertView == null) {
                view = mInflater.inflate(RESOURCE_ID, parent, false)
                val vh = ViewHolder(view.deviceAddress, view.deviceAddressType, view.deviceName)
                view.tag = vh
                vh
            } else {
                view = convertView
                view.tag as ViewHolder
            }
            vh.addr.text = wrapped.device.address
            vh.addrType.text = "(${wrapped.device.addressType()})"
            vh.name.text = wrapped.name
            return view
        }
    }

    data class  WrappedDevice(val device: BluetoothDevice, val name: String)

    data class ViewHolder(val addr: TextView, val addrType: TextView, val name: TextView)

    private val mReceiver = object : BroadcastReceiver() {
        override fun onReceive(ctx: Context?, intent: Intent?) {
            intent?.let {
                when(it.action) {
                    BLEService.ACTION_DEVICE_DISCOVERED -> {
                        val device: BluetoothDevice = it.getParcelableExtra(BLEService.EXTRA_DEVICE)
                        val name = if (it.hasExtra(BLEService.EXTRA_DEVICE_NAME)) {
                            it.getStringExtra(BLEService.EXTRA_DEVICE_NAME)
                        } else {
                            if (device.name != null) {
                                device.name
                            } else {
                                "N/A"
                            }
                        }
                        mDevicesAdapter.add(WrappedDevice(device, name))
                    }
                    else -> {
                        logD("Unhandled ${it.action}")
                    }
                }
            }
        }
    }
}

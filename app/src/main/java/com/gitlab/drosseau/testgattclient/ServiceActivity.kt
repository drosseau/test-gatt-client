package com.gitlab.drosseau.testgattclient

import android.bluetooth.BluetoothGattCharacteristic
import android.bluetooth.BluetoothGattDescriptor
import android.bluetooth.BluetoothGattService
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.ParcelUuid
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.EditText
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_service.*
import kotlinx.android.synthetic.main.simple_uuid_item.view.*
import java.nio.ByteBuffer
import java.nio.charset.CharacterCodingException
import java.nio.charset.Charset
import java.nio.charset.CodingErrorAction
import java.nio.charset.StandardCharsets

class ServiceActivity : AppCompatActivity() {

    private lateinit var mDeviceAddress: String
    private lateinit var mService: BluetoothGattService
    private lateinit var mCharacteristic: BluetoothGattCharacteristic
    private var mDescriptor: BluetoothGattDescriptor? = null
    private lateinit var mCharacteristics: List<BluetoothGattCharacteristic>

    companion object {
        const val EXTRA_SERVICE = "EXTRA_SERVICE"
        const val EXTRA_DEVICE_ADDRESS = "EXTRA_DEVICE_ADDRESS"

        const val PARENT_RESOURCE_ID = R.layout.simple_uuid_item
        const val CHILD_RESOURCE_ID = R.layout.simple_uuid_item
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_service)

        mService = intent.getParcelableExtra(EXTRA_SERVICE)
        mDeviceAddress = intent.getStringExtra(EXTRA_DEVICE_ADDRESS)

        serviceUuid.text = mService.uuid.toString()

        writeCharBtn.isEnabled = false
        writeDescBtn.isEnabled = false
        characteristicValue.isEnabled = false
        descriptorValue.isEnabled = false

        mCharacteristics = mService.characteristics

        if (mCharacteristics.isNotEmpty()) {
            mCharacteristic = mCharacteristics[0]
            changeCharacteristic(mCharacteristic)
            if (mCharacteristic.descriptors.isNotEmpty()) {
                mDescriptor = mCharacteristic.descriptors[0]
                changeDescriptor(mDescriptor!!)
            }
        }

        characteristicsList.setOnGroupClickListener { elv, _, groupPos, _ ->
            val char = mCharacteristics[groupPos]
            if (elv.isGroupExpanded(groupPos)) {
                elv.collapseGroup(groupPos)
                descriptorValue.setText("")
                descriptorUuid.text = getString(R.string.not_applicable)
            } else {
                changeCharacteristic(char)
                elv.expandGroup(groupPos)
            }
            return@setOnGroupClickListener true
        }

        characteristicsList.setAdapter(ExpandableAdapter(this))

        characteristicsList.setOnChildClickListener { _, _, groupPos, childPos, _ ->
            val char = mCharacteristics[groupPos]
            if (char.uuid != mCharacteristic.uuid) {
                changeCharacteristic(char, false)
            }
            val desc = char.descriptors[childPos]
            changeDescriptor(desc)
            return@setOnChildClickListener true
        }

        writeCharBtn.setOnClickListener {
            val intent = BLEService.makeBroadcastIntent(this@ServiceActivity, BLEService.ACTION_WRITE_CHAR)
            val newValue = characteristicValue.text.toString().displayableToBytes()
            logD("Changing ${mCharacteristic.uuid} value to ${characteristicValue.text}")
            mCharacteristic.value = newValue
            intent.putExtra(BLEService.EXTRA_SERVICE_UUID, ParcelUuid(mCharacteristic.service.uuid))
            intent.putExtra(BLEService.EXTRA_CHAR_UUID, ParcelUuid(mCharacteristic.uuid))
            intent.putExtra(BLEService.EXTRA_DATA, newValue)
            sendBroadcast(intent)
        }

        writeDescBtn.setOnClickListener {
            mDescriptor?.let { desc ->
                val intent = BLEService.makeBroadcastIntent(this@ServiceActivity, BLEService.ACTION_WRITE_DESC)
                mDescriptor!!.value = descriptorValue.text.toString().displayableToBytes()
                intent.putExtra(BLEService.EXTRA_SERVICE_UUID, ParcelUuid(desc.characteristic.service.uuid))
                intent.putExtra(BLEService.EXTRA_CHAR_UUID, ParcelUuid(desc.characteristic.uuid))
                intent.putExtra(BLEService.EXTRA_DESC_UUID, ParcelUuid(desc.uuid))
                intent.putExtra(BLEService.EXTRA_DATA, mDescriptor!!.value)
                sendBroadcast(intent)
            }
        }
    }

    private fun changeCharacteristic(char: BluetoothGattCharacteristic, updateDesc: Boolean = true) {
        val old = mCharacteristic.uuid
        mCharacteristic = char
        if (mCharacteristic.isWritable()) {
            logD("Characteristic ${mCharacteristic.uuid} is writable")
            writeCharBtn.isEnabled = true
            characteristicValue.isEnabled = true
        } else {
            logD("Characteristic ${mCharacteristic.uuid} is not writable")
            writeCharBtn.isEnabled = false
            characteristicValue.isEnabled = false
        }
        characteristicUuid.text = mCharacteristic.uuid.toString()
        characteristicValue.setText("")
        if (updateDesc && char.uuid != old) {
            descriptorUuid.text = getString(R.string.not_applicable)
            descriptorValue.setText("")
        }
        readSelectedCharacteristic()
    }

    private fun changeDescriptor(desc: BluetoothGattDescriptor) {
        mDescriptor = desc
        mDescriptor?.let {
            if (it.isWritable()) {
                logD("Descriptor ${it.uuid} is writable")
                writeDescBtn.isEnabled = true
                descriptorValue.isEnabled = true
            } else {
                logD("Descriptor ${it.uuid} is not writable")
                writeDescBtn.isEnabled = false
                descriptorValue.isEnabled = false
            }
            descriptorUuid.text = it.uuid.toString()
        }
        descriptorValue.setText("")
        readSelectedDescriptor()
    }

    override fun onResume() {
        super.onResume()
        val filter = IntentFilter()
        filter.addAction(BLEService.ACTION_CHAR_DATA_READY)
        filter.addAction(BLEService.ACTION_DESC_DATA_READY)
        filter.addAction(BLEService.ACTION_GATT_DISCONNECTED)
        registerReceiver(mReceiver, filter, CLIENT_PERMISSION, null)
    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(mReceiver)
    }

    private fun readSelectedCharacteristic() {
        if (!mCharacteristic.isReadable()) {
            logE("Cannot read characteristic ${mCharacteristic.uuid}")
            return
        }
        val intent = BLEService.makeBroadcastIntent(this, BLEService.ACTION_READ_CHAR)
        intent.putExtra(BLEService.EXTRA_CHAR_UUID, ParcelUuid(mCharacteristic.uuid))
        intent.putExtra(BLEService.EXTRA_SERVICE_UUID, ParcelUuid(mCharacteristic.service.uuid))
        sendBroadcast(intent)
    }

    private fun readSelectedDescriptor() {
        if (mDescriptor == null) {
            logE("No descriptor selected")
            return
        }
        mDescriptor?.let {
            if (!it.isReadable()) {
                logE("Cannot read descriptor ${it.uuid}")
                return
            }
            val intent = BLEService.makeBroadcastIntent(this, BLEService.ACTION_READ_DESC)
            intent.putExtra(BLEService.EXTRA_SERVICE_UUID, ParcelUuid(it.characteristic.service.uuid))
            intent.putExtra(BLEService.EXTRA_CHAR_UUID, ParcelUuid(it.characteristic.uuid))
            intent.putExtra(BLEService.EXTRA_DESC_UUID, ParcelUuid(it.uuid))
            sendBroadcast(intent)
        }
    }

    private val mReceiver = object : BroadcastReceiver() {
        override fun onReceive(ctx: Context?, intent: Intent?) {
            intent?.let {
                when (it.action) {
                    BLEService.ACTION_CHAR_DATA_READY -> {
                        val data = it.getByteArrayExtra(BLEService.EXTRA_DATA)
                        logD("Setting char data to ${data.toDisplayable()}")
                        characteristicValue.setText(data.toDisplayable())
                    }
                    BLEService.ACTION_DESC_DATA_READY -> {
                        val data = it.getByteArrayExtra(BLEService.EXTRA_DATA)
                        logD("Setting desc data to ${data.toDisplayable()}")
                        descriptorValue.setText(data.toDisplayable())
                    }
                    BLEService.ACTION_GATT_DISCONNECTED -> {
                        finish()
                    }
                    else -> {
                        logI("Unhandled action: ${it.action}")
                    }
                }
            }
        }


    }

    private inner class ExpandableAdapter(ctx: Context) : BaseExpandableListAdapter() {
        private val mInflater = LayoutInflater.from(ctx)
        override fun getGroup(groupPos: Int): Any {
            return mCharacteristics[groupPos].descriptors
        }

        override fun isChildSelectable(groupPos: Int, childPos: Int): Boolean {
            return true
        }

        override fun hasStableIds(): Boolean {
            return true
        }

        override fun getGroupView(groupPos: Int, isExpand: Boolean, convertView: View?, parent: ViewGroup?): View {
            val char = mCharacteristics[groupPos]
            val view: View
            val vh = if (convertView == null) {
                view = mInflater.inflate(PARENT_RESOURCE_ID, parent, false)
                val vh = ParentViewHolder(view.uuid)
                view.tag = vh
                vh
            } else {
                view = convertView
                view.tag as ParentViewHolder
            }
            vh.charUuid.text = char.uuid.toString()
            return view
        }

        override fun getChildrenCount(groupPos: Int): Int {
            return mCharacteristics[groupPos].descriptors.size
        }

        override fun getChild(groupPos: Int, childPos: Int): Any {
            return mCharacteristics[groupPos].descriptors[childPos]
        }

        override fun getGroupId(groupPos: Int): Long {
            val uuid = mCharacteristics[groupPos].uuid
            return uuid.leastSignificantBits.xor(uuid.mostSignificantBits)
        }

        override fun getChildView(groupPos: Int, childPos: Int, isExpanded: Boolean, convertView: View?, parent: ViewGroup?): View {
            val desc = mCharacteristics[groupPos].descriptors[childPos]
            val view: View
            val vh = if (convertView == null) {
                view = mInflater.inflate(CHILD_RESOURCE_ID, parent, false)
                val vh = ChildViewHolder(view.uuid)
                view.tag = vh
                vh
            } else {
                view = convertView
                view.tag as ChildViewHolder
            }
            vh.descUuid.text = desc.uuid.toString()
            return view
        }

        override fun getGroupCount(): Int {
            return mCharacteristics.size
        }

        override fun getChildId(groupPos: Int, childPos: Int): Long {
            val uuid = mCharacteristics[groupPos].descriptors[childPos].uuid
            return uuid.leastSignificantBits.xor(uuid.mostSignificantBits)
        }
    }

    private data class ParentViewHolder(val charUuid: TextView)
    private data class ChildViewHolder(val descUuid: TextView)
}

package com.gitlab.drosseau.testgattclient

enum class AddressType {
    STATIC, RANDOM, RESOLVABLE_PRIVATE, NONRESOLVABLE_PRIVATE;

    override fun toString() : String {
        return when(this) {
            STATIC -> "Static"
            RANDOM -> "Random"
            RESOLVABLE_PRIVATE -> "RPA"
            NONRESOLVABLE_PRIVATE -> "NRPA"
        }
    }

}

package com.gitlab.drosseau.testgattclient

import android.app.Activity
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothGattCharacteristic
import android.bluetooth.BluetoothGattDescriptor
import android.content.Context
import android.util.Log
import android.widget.EditText
import android.widget.Toast
import java.nio.ByteBuffer
import java.nio.CharBuffer
import java.nio.charset.CharacterCodingException
import java.nio.charset.Charset
import java.nio.charset.CodingErrorAction
import java.nio.charset.StandardCharsets

private const val LOG_TAG = "TestGattClient"
const val PKG_PREFIX = "com.gitlab.drosseau.testgattclient"

fun Activity.toast(msg: CharSequence, len: Int = Toast.LENGTH_SHORT) {
    runOnUiThread {
        logI(msg.toString())
        Toast.makeText(this, msg, len).show()
    }
}

fun logE(msg: String) {
    Log.e(LOG_TAG, msg)
}

fun logI(msg: String) {
    Log.i(LOG_TAG, msg)
}

fun logD(msg: String) {
    Log.d(LOG_TAG, msg)
}

private val HEX_CHARS = "0123456789abcdef".toCharArray()

fun List<Byte>.toDisplayable(): String {
    if (size == 0) {
        return ""
    }
    if (any { it < 0x20 || it.toInt() == 0x7f }) {
        return "0x${toHex()}"
    }
    return toByteArray().toString(Charset.defaultCharset())
}

fun ByteArray.toDisplayable(): String {
    return toList().toDisplayable()
}

fun List<Byte>.toHex(): String {
    if (size == 0) {
        return ""
    }
    val hex = CharArray(size * 2)
    for (i in 0 until size) {
        val bv = this[i].toInt().and(0xFF)
        hex[i.shl(1)] = HEX_CHARS[bv.ushr(4).and(0xF)]
        hex[i.shl(1) + 1] = HEX_CHARS[bv.and(0xF)]
    }
    return String(hex)
}

fun ByteArray.toHex(): String {
    return toList().toHex()
}

fun List<Byte>.toBinary(): String {
    if (size == 0) {
        return ""
    }

    val builder = StringBuilder()
    for (b in this) {
        var asBin = Integer.toBinaryString(b.toInt())
        if (asBin.length != 8) {
            asBin = asBin.padStart(8 - asBin.length, '0')
        }
        builder.append(asBin)
    }
    return builder.toString()
}

fun ByteArray.toBinary(): String {
    return toList().toBinary()
}

private fun hexCharIdx(c: Char): Int {
    for (i in 0.until(16)) {
        if (c == HEX_CHARS[i]) {
            return i
        }
    }
    throw IllegalArgumentException("couldn't find $c in $HEX_CHARS")
}

fun String.displayableToBytes(): ByteArray {
    if (startsWith("0x")) {
        return hexToBytes()
    }
    return toByteArray(Charset.defaultCharset())
}

fun String.hexToBytes(): ByteArray {
    if (this.length.and(1) != 0) {
        throw IllegalArgumentException("length of $this is not divisible by 2")
    }
    val b = ByteArray(this.length / 2)
    val lc = this.toLowerCase()
    var j = 0
    for (i in 0.until(lc.length).step(2)) {
        val idx = hexCharIdx(lc[i])
        val idx2 = hexCharIdx(lc[i + 1])
        b[j] = idx.and(0x0F).shl(4).or(idx2.and(0x0F)).toByte()
        j++
    }
    return b
}

fun BluetoothDevice.addressType(): AddressType {
    val sigByte = address.slice(0..1).toInt(16)
    val msb = sigByte.and(0x80).shr(7)
    val sMsb = sigByte.and(0x40).shr(6)
    return if (msb == 0) {
        if (sMsb == 0) {
            AddressType.NONRESOLVABLE_PRIVATE
        } else {
            AddressType.RESOLVABLE_PRIVATE
        }
    } else {
        if (sMsb == 1) {
            AddressType.RANDOM
        } else {
            AddressType.STATIC
        }
    }
}

/**
 * In the following 4 methods I have commented out everything to do with the
 * permissions values. This is because these are not set at all after service
 * discovery. In fact, Android doesn't ever really seem to care about these and
 * handles permissions by just trying and hoping.
 */

fun BluetoothGattCharacteristic.isReadable(): Boolean {
    /*
    val perms = permissions
    val hasReadPermission = (
        perms.and(BluetoothGattCharacteristic.PERMISSION_READ) or
        perms.and(BluetoothGattCharacteristic.PERMISSION_READ_ENCRYPTED) or
        perms.and(BluetoothGattCharacteristic.PERMISSION_READ_ENCRYPTED_MITM)
    ) != 0
    */
    val hasReadPermission = true

    val props = properties
    val hasReadProperty = props.and(BluetoothGattCharacteristic.PROPERTY_READ) != 0

    return hasReadPermission && hasReadProperty
}

fun BluetoothGattCharacteristic.isWritable(): Boolean {
    val props = properties
    val hasWritePermission = true
    val hasWriteProperty = props.and(BluetoothGattCharacteristic.PROPERTY_WRITE) != 0
    return hasWritePermission && hasWriteProperty
}

fun BluetoothGattDescriptor.isReadable(): Boolean {
    return true
    /*
    val perms = permissions
    return (
        perms.and(BluetoothGattDescriptor.PERMISSION_READ) or
        perms.and(BluetoothGattDescriptor.PERMISSION_READ_ENCRYPTED) or
        perms.and(BluetoothGattDescriptor.PERMISSION_READ_ENCRYPTED_MITM)
    ) != 0
    */
}

fun BluetoothGattDescriptor.isWritable(): Boolean {
    return true
}

/**
 * The following two new methods on EditTexts are because we are going to be
 * dealing with both binary and nonbinary data.
 */

fun EditText.putBytes(buf: ByteArray) {
    setText(try {
        StandardCharsets.UTF_8.newDecoder()
                .onMalformedInput(CodingErrorAction.REPORT)
                .decode(ByteBuffer.wrap(buf))
    } catch (e: CharacterCodingException) {
        "0x${buf.toHex()}"
    } as CharSequence)
}

fun EditText.getBytes(): ByteArray {
    return if (text.startsWith("0x")) {
        text.toString().substring(2).hexToBytes()
    } else {
        StandardCharsets.UTF_8.newEncoder()
                .encode(text as CharBuffer)
                .array()
    }
}

const val CLIENT_PERMISSION = "com.gitlab.drosseau.testgattclient.GATT_CLIENT_PERMISSION"
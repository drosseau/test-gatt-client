package com.gitlab.drosseau.testgattclient

import android.app.Service
import android.bluetooth.*
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanResult
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Binder
import android.os.IBinder
import android.os.ParcelUuid
import java.util.*

class BLEService : Service() {

    private lateinit var mBluetoothManager: BluetoothManager
    private lateinit var mBluetoothAdapter: BluetoothAdapter
    private var mBluetoothGatt: BluetoothGatt? = null

    private var mConnectionState = ConnState.DISCONNECTED
    private val mFoundDevices: MutableSet<BluetoothDevice> = mutableSetOf()

    private val mServices: MutableSet<BluetoothGattService> = mutableSetOf()

    private enum class ConnState {
        DISCONNECTED,
        CONNECTING,
        CONNECTED,
    }

    override fun onCreate() {
        logD("BLEService onCreate")
        super.onCreate()
        mBluetoothManager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        mBluetoothAdapter = mBluetoothManager.adapter
        val filter = IntentFilter()
        filter.addAction(ACTION_WRITE_CHAR)
        filter.addAction(ACTION_WRITE_DESC)
        filter.addAction(ACTION_READ_DESC)
        filter.addAction(ACTION_READ_CHAR)
        filter.addAction(ACTION_BEGIN_SCANNING)
        filter.addAction(ACTION_END_SCANNING)
        filter.addAction(ACTION_CONNECT_DEVICE)
        filter.addAction(ACTION_DISCONNECT_DEVICE)
        registerReceiver(
                mReceiver, filter, CLIENT_PERMISSION, null
        )
    }

    override fun onDestroy() {
        logD("BLEService onDestroy")
        super.onDestroy()
        disconnectDevice()
        unregisterReceiver(mReceiver)
    }

    private fun disconnectDevice() {
        mBluetoothGatt?.let {
            mConnectionState = ConnState.DISCONNECTED
            it.disconnect()
            it.close()
            mBluetoothGatt = null
        }
    }

    fun isConnected(): Boolean {
        return mConnectionState == ConnState.CONNECTED
    }

    fun getServices(): List<BluetoothGattService> {
        return mServices.toList()
    }

    fun openDeviceConnection(address: String): Boolean {
        if (mConnectionState == ConnState.CONNECTED) {
            logI("We were already connected somehow")
            mBluetoothGatt?.disconnect()
            mBluetoothGatt?.close()
            mConnectionState = ConnState.DISCONNECTED
        }

        mBluetoothGatt?.close()

        val device = mBluetoothAdapter.getRemoteDevice(address)
        if (device == null) {
            logE("Couldn't connect to device with address: $address")
            return false
        }
        logI("Connecting to $address")
        mBluetoothGatt = device.connectGatt(this, false, mGattCallback)
        mConnectionState = ConnState.CONNECTING
        return true
    }

    companion object {
        private const val PREFIX = "$PKG_PREFIX.BLEService"

        const val ACTION_GATT_CONNECTED = "$PREFIX.ACTION_GATT_CONNECTED"
        const val ACTION_GATT_DISCONNECTED = "$PREFIX.ACTION_GATT_DISCONNECTED"
        const val ACTION_GATT_SERVICES_DISCOVERED = "$PREFIX.ACTION_GATT_SERVICES_DISCOVERED"
        const val ACTION_CONNECT_DEVICE = "$PREFIX.ACTION_CONNECT_DEVICE"
        const val ACTION_DISCONNECT_DEVICE = "$PREFIX.ACTION_DISCONNECT_DEVICE"
        const val ACTION_READ_CHAR = "$PREFIX.ACTION_READ_CHAR"
        const val ACTION_WRITE_CHAR = "$PREFIX.ACTION_WRITE_CHAR"
        const val ACTION_READ_DESC = "$PREFIX.ACTION_READ_DESC"
        const val ACTION_WRITE_DESC = "$PREFIX.ACTION_WRITE_DESC"
        const val ACTION_BEGIN_SCANNING = "$PREFIX.ACTION_BEGIN_SCANNING"
        const val ACTION_END_SCANNING = "$PREFIX.ACTION_END_SCANNING"
        const val ACTION_DEVICE_DISCOVERED = "$PREFIX.ACTION_DEVICE_DISCOVERED"
        const val ACTION_CHAR_DATA_READY = "$PREFIX.ACTION_CHAR_DATA_READY"
        const val ACTION_DESC_DATA_READY = "$PREFIX.ACTION_DESC_DATA_READY"

        const val EXTRA_DATA = "EXTRA_DATA"
        const val EXTRA_DEVICE = "EXTRA_DEVICE"
        const val EXTRA_DEVICE_NAME = "EXTRA_DEVICE_NAME"
        const val EXTRA_DEVICE_ADDRESS = "EXTRA_DEVICE_ADDRESS"
        const val EXTRA_SERVICE_UUID = "EXTRA_SERVICE_UUID"
        const val EXTRA_CHAR_UUID = "EXTRA_CHAR_UUID"
        const val EXTRA_DESC_UUID = "EXTRA_DESC_UUID"

        private val PROFILE_SERVICE = UUID.fromString("00001800-0000-1000-8000-00805f9b34fb")
        private val NAME_CHARACTERISTIC = UUID.fromString("00002a00-0000-1000-8000-00805f9b34fb")

        fun makeBroadcastIntent(ctx: Context, action: String): Intent {
            val intent = Intent()
            intent.setPackage(ctx.packageName)
            intent.action = action
            return intent
        }
    }

    private val mBinder = LocalBinder()

    override fun onBind(intent: Intent?): IBinder {
        return mBinder
    }

    inner class LocalBinder : Binder() {
        fun getService(): BLEService {
            return this@BLEService
        }
    }

    private fun writeChar(char: BluetoothGattCharacteristic) {
        if (mBluetoothGatt == null) {
            logE("Tried to write with null gatt")
            return
        }
        mBluetoothGatt?.writeCharacteristic(char)
    }

    private fun readChar(char: BluetoothGattCharacteristic) {
        logD("Reading char ${char.uuid} in service ${char.service.uuid}")
        if (mBluetoothGatt == null) {
            logE("Tried to read with null gatt")
            return
        }
        mBluetoothGatt?.let {
            if (!it.readCharacteristic(char)) {
                logE("Failed to read ${char.uuid}")
            }
        }
    }

    private fun writeDesc(desc: BluetoothGattDescriptor) {
        if (mBluetoothGatt == null) {
            logE("Tried to write with null gatt")
            return
        }
        mBluetoothGatt?.writeDescriptor(desc)
    }

    private fun readDesc(desc: BluetoothGattDescriptor) {
        logD("Reading desc ${desc.uuid}")
        if (mBluetoothGatt == null) {
            logE("Tried to read with null gatt")
            return
        }
        mBluetoothGatt?.let {
            if (!it.readDescriptor(desc)) {
                logE("Failed to read ${desc.uuid}")
            }
        }
    }

    private val mReceiver = BLEServiceReceiver()

    private fun descriptorFromIntent(it: Intent): BluetoothGattDescriptor? {
        val desc: UUID? = (it.getParcelableExtra(EXTRA_DESC_UUID) as ParcelUuid?)?.uuid
        if (desc == null) {
            logE("Descriptor UUID was null")
            return null
        }
        val char = characteristicFromIntent(it)
        if (char == null) {
            logE("Couldn't find characteristic for descriptor $desc")
            return null
        }
        for (d in char.descriptors) {
            if (d.uuid == desc) {
                logD("Found descriptor ${d.uuid}")
                return d
            }
        }
        logE("Couldn't find descriptor $desc in characteristic ${char.uuid}")
        return null
    }

    private fun characteristicFromIntent(it: Intent): BluetoothGattCharacteristic? {
        val char: UUID? = (it.getParcelableExtra(EXTRA_CHAR_UUID) as ParcelUuid?)?.uuid
        val service: UUID? = (it.getParcelableExtra(EXTRA_SERVICE_UUID) as ParcelUuid?)?.uuid
        if (char == null) {
            logE("Characteristic UUID was null")
            return null
        }
        if (service == null) {
            logE("Service UUID was null")
            return null
        }
        for (s in mServices) {
            if (s.uuid == service) {
                for (c in s.characteristics) {
                    if (c.uuid == char) {
                        return c
                    }
                }
                logE("Couldn't find characteristic")
                return null
            }
        }
        return null
    }

    inner class BLEServiceReceiver : BroadcastReceiver() {
        override fun onReceive(ctx: Context?, intent: Intent?) {
            intent?.let {
                logD("BLEService receiver got ${it.action}")
                when (it.action) {
                    ACTION_WRITE_DESC -> {
                        descriptorFromIntent(it)?.let { desc ->
                            desc.value = it.getByteArrayExtra(EXTRA_DATA)
                            writeDesc(desc)
                        }
                    }
                    ACTION_READ_DESC -> {
                        descriptorFromIntent(it)?.let { desc ->
                            readDesc(desc)
                        }
                    }
                    ACTION_WRITE_CHAR -> {
                        characteristicFromIntent(it)?.let { char ->
                            char.value = it.getByteArrayExtra(EXTRA_DATA)
                            writeChar(char)
                        }
                    }
                    ACTION_READ_CHAR -> {
                        characteristicFromIntent(it)?.let { char ->
                            readChar(char)
                        }
                    }
                    ACTION_CONNECT_DEVICE -> {
                        val address = it.getStringExtra(EXTRA_DEVICE_ADDRESS)
                        logD("Connecting to $address")
                        openDeviceConnection(address)
                    }
                    ACTION_DISCONNECT_DEVICE -> {
                        disconnectDevice()
                    }
                    ACTION_BEGIN_SCANNING -> {
                        logD("Starting scan")
                        mFoundDevices.clear()
                        val scanner = mBluetoothAdapter.bluetoothLeScanner
                        scanner.startScan(mScanCallback)
                    }
                    ACTION_END_SCANNING -> {
                        logD("Ending scan")
                        val scanner = mBluetoothAdapter.bluetoothLeScanner
                        scanner.stopScan(mScanCallback)
                    }
                    else -> {
                        logE("Unhandled action: ${it.action}")
                    }
                }
            }
        }
    }

    private val mScanCallback = object : ScanCallback() {

        override fun onScanFailed(errorCode: Int) {
            logE("Scan failed with code $errorCode")
            super.onScanFailed(errorCode)
        }

        override fun onScanResult(callbackType: Int, result: ScanResult?) {
            logD("Got scan result")
            result?.let {
                if (mFoundDevices.add(it.device)) {
                    /*
                    if (it.device.name == null || it.device.name.isEmpty()) {
                        it.device.connectGatt(this@BLEService, false, mDeviceNameCallback)
                    } else {
                    */
                    logI("Found device ${it.device.name} at ${it.device.address}")
                    val intent = Intent()
                    intent.setPackage(this@BLEService.packageName)
                    intent.action = ACTION_DEVICE_DISCOVERED
                    intent.putExtra(EXTRA_DEVICE, it.device)
                    sendBroadcast(intent)
                    //}
                }
            }
            super.onScanResult(callbackType, result)
        }
    }

    /**
     * TODO I think this is done by Android automagically.
     */
    private val mDeviceNameCallback = object : BluetoothGattCallback() {
        override fun onConnectionStateChange(gatt: BluetoothGatt?, status: Int, newState: Int) {
            when (newState) {
                BluetoothProfile.STATE_CONNECTED -> {
                    gatt?.let {
                        val service = BluetoothGattService(
                                PROFILE_SERVICE,
                                BluetoothGattService.SERVICE_TYPE_PRIMARY
                        )
                        val char = BluetoothGattCharacteristic(
                                NAME_CHARACTERISTIC,
                                BluetoothGattCharacteristic.PROPERTY_READ,
                                BluetoothGattCharacteristic.PERMISSION_READ
                        )
                        service.addCharacteristic(char)
                        it.readCharacteristic(char)
                    }
                }
            }
            super.onConnectionStateChange(gatt, status, newState)
        }

        override fun onCharacteristicRead(gatt: BluetoothGatt?, characteristic: BluetoothGattCharacteristic?, status: Int) {
            val intent = Intent()
            intent.setPackage(this@BLEService.packageName)
            intent.action = ACTION_DEVICE_DISCOVERED
            intent.putExtra(EXTRA_DEVICE, gatt?.device)
            if (status != BluetoothGatt.GATT_SUCCESS) {
                characteristic?.let {
                    intent.putExtra(EXTRA_DEVICE_NAME, it.value.toString())
                }
            }
            sendBroadcast(intent)
        }
    }

    private val mGattCallback = object : BluetoothGattCallback() {
        override fun onConnectionStateChange(gatt: BluetoothGatt?, status: Int, newState: Int) {
            super.onConnectionStateChange(gatt, status, newState)
            if (status != BluetoothGatt.GATT_SUCCESS) {
                logE("Bad status onConnectionStateChange: $status")
                mConnectionState = ConnState.DISCONNECTED
                broadcast(ACTION_GATT_DISCONNECTED)
                return
            }
            if (gatt == null) {
                logE("gatt was null")
                return
            }
            when (newState) {
                BluetoothProfile.STATE_CONNECTED -> {
                    logI("Connected")
                    mConnectionState = ConnState.CONNECTED
                    broadcast(ACTION_GATT_CONNECTED)
                    logI("Starting service discovery...")
                    mBluetoothGatt!!.discoverServices()
                    //gatt.discoverServices()
                }
                BluetoothProfile.STATE_DISCONNECTED -> {
                    logI("Disconnected")
                    mConnectionState = ConnState.DISCONNECTED
                    broadcast(ACTION_GATT_DISCONNECTED)
                }
                else -> {
                    logD("Unhandled state: $newState")
                }
            }
        }

        override fun onServicesDiscovered(gatt: BluetoothGatt?, status: Int) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                logI("Services discovered")
                mServices.clear()
                // TODO: I don't know why duplicates are being found and not
                // filtered by the set....
                mBluetoothGatt?.let {
                    for (s in it.services) {
                        var seen = false
                        for (found in mServices) {
                            if (s.uuid == found.uuid) {
                                seen = true
                                break
                            }
                        }
                        if (!seen) {
                            mServices.add(s)
                        }
                    }
                }
                broadcast(ACTION_GATT_SERVICES_DISCOVERED)
            } else {
                logE("Failed to discover services: $status")
            }
            super.onServicesDiscovered(gatt, status)
        }

        override fun onCharacteristicRead(gatt: BluetoothGatt?, char: BluetoothGattCharacteristic?, status: Int) {
            logD("onCharacteristicRead status: $status")
            char?.let {
                val bytes = char.value
                logD("Characteristic ${char.uuid} value is ${bytes.toDisplayable()}")
                broadcast(ACTION_CHAR_DATA_READY, bytes)
            }
            super.onCharacteristicRead(gatt, char, status)
        }

        override fun onDescriptorRead(gatt: BluetoothGatt?, desc: BluetoothGattDescriptor?, status: Int) {
            logD("onDescriptorRead status: $status")
            desc?.let {
                val bytes = desc.value
                logD("Descriptor ${desc.uuid} value is ${bytes.toDisplayable()}")
                broadcast(ACTION_DESC_DATA_READY, bytes)
            }
            super.onDescriptorRead(gatt, desc, status)
        }

        override fun onCharacteristicWrite(gatt: BluetoothGatt?, char: BluetoothGattCharacteristic?, status: Int) {
            if (status != BluetoothGatt.GATT_SUCCESS) {
                logE("Failed to write characteristic")
            }
            super.onCharacteristicWrite(gatt, char, status)
        }

        override fun onDescriptorWrite(gatt: BluetoothGatt?, desc: BluetoothGattDescriptor?, status: Int) {
            if (status != BluetoothGatt.GATT_SUCCESS) {
                logE("Failed to write descriptor")
            }
            super.onDescriptorWrite(gatt, desc, status)
        }
    }

    private fun broadcast(action: String, dat: ByteArray? = null) {
        val intent = Intent(action)
        intent.setPackage(this.packageName)
        if (dat != null && dat.isNotEmpty()) {
            intent.putExtra(EXTRA_DATA, dat)
        }
        sendBroadcast(intent)
    }
}
